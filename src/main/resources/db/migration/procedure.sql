CREATE FUNCTION count_set_bits(bit_string INTEGER) RETURNS INTEGER
AS
$$
select length(replace(x::TEXT, '0', ''))
from (values (bit_string::bit varying)) as something(x);
$$
    LANGUAGE SQL
;

