package bsa.java.concurrency.image.controller;

import bsa.java.concurrency.exception.CannotCaculateImageHashException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.service.ImageService;
import bsa.java.concurrency.util.MultipartFileConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/image")
@RequiredArgsConstructor
public class ImageController {
    private final ImageService imageService;

    private final MultipartFileConverter fileConverter;

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public void badRequest() {
        // Nothing to do
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(CannotCaculateImageHashException.class)
    public void internalServerError() {
        // Nothing to do
    }

    @ResponseStatus(value = HttpStatus.INSUFFICIENT_STORAGE)
    @ExceptionHandler(IOException.class)
    public void insufficientStorage() {
        // Nothing to do
    }

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        imageService.batchUploadImages(fileConverter.fileArrayToBytes(files),
                ServletUriComponentsBuilder.fromCurrentContextPath().build().toUri()).join();
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file, @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) throws ExecutionException, InterruptedException, IOException {
        return imageService.searchMatches(file.getBytes(), threshold, ServletUriComponentsBuilder.fromCurrentContextPath().build().toUri()).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        imageService.deleteById(imageId).join();
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages() {
        imageService.deleteAll().join();
    }
}
