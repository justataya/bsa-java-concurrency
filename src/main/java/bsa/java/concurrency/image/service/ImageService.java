package bsa.java.concurrency.image.service;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.ImageEntity;
import bsa.java.concurrency.image.repository.ImageRepository;
import bsa.java.concurrency.util.Hasher;
import bsa.java.concurrency.util.ImageMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
@Transactional
public class ImageService {
    private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

    private final ImageRepository imageRepository;

    private final ImageMapper imageMapper;

    @Qualifier("diagonalHasher")
    private final Hasher hasher;

    @Qualifier("fileSystem")
    private final FileSystem fileSystem;

    @Async
    public CompletableFuture<Void> batchUploadImages(List<byte[]> files, URI uri) {
        logger.info("Batch upload images");
        for (var bytes : files) {
            try {
                var uuid = UUID.randomUUID();

                AtomicReference<String> path = new AtomicReference<>();

                AtomicLong hash = new AtomicLong();

                return CompletableFuture.allOf(
                        computeHash(bytes).thenAccept(hash::set),
                        fileSystem.saveFile(uuid.toString(), bytes, uri).thenAccept(path::set))
                        .thenAccept(value -> imageRepository.save(
                                ImageEntity.builder().uuid(uuid).hash(hash.get()).path(path.get()).build()));
            } catch (IOException exception) {
                logger.error(exception.getMessage());
            }
        }

        return CompletableFuture.completedFuture(null);
    }

    @Async
    public CompletableFuture<List<SearchResultDTO>> searchMatches(byte[] bytes, double threshold, URI uri) {
        logger.info("Search image matches");

        if (threshold < 0.0 || threshold > 1.0) {
            throw new IllegalArgumentException("Threshold must be in range [0.0,1.0]");
        }

        AtomicLong hash = new AtomicLong();

        return computeHash(bytes).thenAccept(hash::set)
                .thenApply(value -> imageRepository.findByHashMatchWithThreshold(hash.get(), threshold))
                .thenApply(result -> {
                    if (result.isEmpty()) {
                        try {
                            return batchUploadImages(List.of(bytes), uri).thenApply((value) -> new ArrayList<SearchResultDTO>()).get();
                        } catch (InterruptedException | ExecutionException e) {
                            logger.error(e.getMessage());
                        }
                    }
                    return imageMapper.entityListToDtoList(result, hash.get());
                });
    }

    @Async
    public CompletableFuture<Void> deleteById(UUID imageId) {
        logger.info("Delete by id " + imageId);

        imageRepository.deleteByUuid(imageId);

        return fileSystem.deleteByPath(imageId.toString());
    }

    @Async
    public CompletableFuture<Void> deleteAll() {
        return CompletableFuture.allOf(CompletableFuture.runAsync(imageRepository::deleteAll),
                fileSystem.deleteAll());
    }

    private CompletableFuture<Long> computeHash(byte[] bytes) {
        return CompletableFuture.supplyAsync(() -> hasher.compute(bytes));
    }
}
