package bsa.java.concurrency.image.repository;

import bsa.java.concurrency.image.entity.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<ImageEntity, Long> {

    @Modifying
    void deleteByUuid(UUID uuid);

    @Query(value = "SELECT * FROM images WHERE (1.0 - count_set_bits(CAST((images.hash # :hash) AS BIT(64))) / 64.0) > :threshold",
            nativeQuery = true)
    List<ImageEntity> findByHashMatchWithThreshold(long hash, double threshold);
}
