package bsa.java.concurrency.fs;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;

@Component(value = "fileSystem")
public class FileSystemImpl implements FileSystem {
    private static final Logger logger = LoggerFactory.getLogger(FileSystemImpl.class);

    @Value("${file.upload-dir}")
    private String dirPath;

    @Override
    public CompletableFuture<String> saveFile(String relativePath, byte[] file, URI uri) throws IOException {
        Path path = checkPath(relativePath + ".jpg");
        System.out.println(path.toString());
        Files.write(path.toAbsolutePath(), file);
        return CompletableFuture.completedFuture(ServletUriComponentsBuilder.fromUri(uri)
                .path(dirPath)
                .path(relativePath)
                .toUriString());
    }

    @Override
    public CompletableFuture<Void> deleteByPath(String relativePath) {
        Path path = Paths.get(dirPath + "/" + relativePath + ".jpg");
        if (Files.exists(path)) {
            return CompletableFuture.runAsync(() -> {
                System.out.println(new File(path.toString()).delete());
            });
        }
        return CompletableFuture.completedFuture(null);
    }

    @Override
    public CompletableFuture<Void> deleteAll() {
        Path path = Paths.get(dirPath);
        if (Files.exists(path)) {
            return CompletableFuture.runAsync(() -> {
                try {
                    FileUtils.cleanDirectory(new File(path.toString()));
                } catch (IOException exception) {
                    logger.error(exception.getMessage());
                }
            });
        }
        return CompletableFuture.completedFuture(null);
    }

    private Path checkPath(String inputPath) throws IOException {
        Path path = Paths.get(dirPath + "/" + inputPath);
        Path parentDir = path.getParent();
        if (!Files.exists(parentDir)) {
            Files.createDirectories(parentDir);
        }
        return path;
    }
}
