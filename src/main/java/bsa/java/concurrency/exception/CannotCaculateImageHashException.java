package bsa.java.concurrency.exception;

public class CannotCaculateImageHashException extends RuntimeException {
    private static final String defaultMessage = "Error calculating image hash";

    public CannotCaculateImageHashException() {
        super(defaultMessage);
    }
}
