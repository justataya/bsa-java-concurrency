package bsa.java.concurrency.util;

public interface Hasher {
    long compute(byte[] bytes);

    double calculateThreshold(long hash1, long hash2);
}
