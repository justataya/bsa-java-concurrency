package bsa.java.concurrency.util;

import bsa.java.concurrency.exception.CannotCaculateImageHashException;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Component(value = "diagonalHasher")
public class DiagonalHasher implements Hasher {
    private static final int width = 9,
            height = 9;

    @Override
    public long compute(byte[] bytes) {
        try {
            var image = ImageIO.read(new ByteArrayInputStream(bytes));
            return calculateDiagonalHash(grayScale(image));
        } catch (IOException exception) {
            throw new CannotCaculateImageHashException();
        }
    }


    private BufferedImage grayScale(BufferedImage image) {
        var result = image.getScaledInstance(9, 9, Image.SCALE_SMOOTH);
        var output = new BufferedImage(9, 9, BufferedImage.TYPE_BYTE_GRAY);
        output.getGraphics().drawImage(result, 0, 0, null);

        return output;
    }

    private long calculateDiagonalHash(BufferedImage image) {
        long hash = 0;
        for (var row = 1; row < width; row++) {
            for (var column = 1; column < height; column++) {
                var prev = brightnessScore(image.getRGB(column - 1, row - 1));
                var current = brightnessScore(image.getRGB(column, row));
                hash |= current > prev ? 1 : 0;
                hash <<= 1;
            }
        }
        return hash;
    }

    private int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }

    public double calculateThreshold(long hash1, long hash2) {
        return 1 - (double) Long.bitCount(hash1 ^ hash2) / 64;
    }
}
