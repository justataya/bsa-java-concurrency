package bsa.java.concurrency.util;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.ImageEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ImageMapper {
    @Qualifier("diagonalHasher")
    private final Hasher hasher;

    public SearchResultDTO entityToDto(ImageEntity imageEntity, long hash) {
        return SearchResultDTO.builder()
                .imageId(imageEntity.getUuid())
                .matchPercent(hasher.calculateThreshold(hash, imageEntity.getHash()))
                .imageUrl(imageEntity.getPath())
                .build();
    }

    public List<SearchResultDTO> entityListToDtoList(List<ImageEntity> imageEntityList, long hash) {
        return imageEntityList.stream().map(item -> entityToDto(item, hash)).collect(Collectors.toList());
    }
}
