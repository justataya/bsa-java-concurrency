package bsa.java.concurrency.util;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class MultipartFileConverter {
    public List<byte[]> fileArrayToBytes(MultipartFile[] files) {
        var result = new ArrayList<byte[]>();

        for (var file : files) {
            try {
                result.add(file.getBytes());
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        return result;
    }

}
